import { createStackNavigator, createAppContainer } from "react-navigation";

import TodoList from '../screen/TodoList';

const MainNavigator = createStackNavigator({
    TodoList: {
        screen: TodoList,
        navigationOptions: ({ navigation }) => ({
            title: `GaweanKu`,
        }),
    }
});

const RootNavigation = createAppContainer(MainNavigator);

export default RootNavigation;