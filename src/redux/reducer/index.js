import { combineReducers } from 'redux';
import { createNavigationReducer } from 'react-navigation-redux-helpers';
import RootNavigation from './../../navigations/RootNavigation';
import todoReducer from './todoReducer';

const router = createNavigationReducer(RootNavigation);

const appReducer = combineReducers({
    router,
    todos: todoReducer
})

export default appReducer