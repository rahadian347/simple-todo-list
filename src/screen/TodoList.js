import React, { Component, Fragment } from 'react'
import { View, Text, TextInput, StyleSheet, Button, Keyboard, TouchableOpacity, Alert } from 'react-native';
import { connect } from 'react-redux';

import * as actionTodos from './../redux/action/todo';


class TodoList extends Component {

	constructor(props) {
		super(props)
		this.state = {
			id: "",
			newTodo: "",
			isHidden: true
		}
	}

	placeSubmitHandler = () => {
		this.props.addTodos({
			id: Math.floor(Math.random() * 9),
			name: this.state.newTodo
		})

		this.setState({ newTodo: "", isHidden: false })
		Keyboard.dismiss()
	}

	handleButtonEdit = () => {
		this.props.editTodos({
			id: this.state.id,
			name: this.state.newTodo
		})

		this.setState({ newTodo: "" })
		Keyboard.dismiss()
	}

	handleChangeText = (text) => {
		this.setState({ newTodo: text })
	}

	handleEdit = (item) => () => {
		this.setState({ newTodo: item.name, id: item.id })
	}
	handleButton = () => () => {
		this.setState({ isHidden: !this.state.isHidden })
	}



	handleRemoveData = (id) => () => {
		// this.props.removeTodos(id)
		Alert.alert(
			'Hapus Task',
			'yakin untuk menghapus task ?',
			[
				{
					text: 'Cancel',
					onPress: () => console.log('Cancel Pressed'),
					style: 'cancel',
				},
				{ text: 'Ok', onPress: () => this.props.removeTodos(id) },
			],
			{ cancelable: false },
		);
	}

	render() {

		return (
			<View style={styles.container}>
				<View style={styles.inputContainer}>
					<TextInput
						placeholder="Todo List"
						style={styles.placeInput}
						value={this.state.newTodo}
						onChangeText={this.handleChangeText}
					></TextInput>
					<Button title='Add'
						style={styles.placeButton}
						onPress={this.placeSubmitHandler}
					/>

					<DisplayView hide={this.state.isHidden}>
						<Button title='Edit'
							style={styles.placeButton}
							onPress={this.handleButtonEdit}
						/>
					</DisplayView>
				</View>

				{this.props.todos.todos.length !== 0 && this.props.todos.todos.map((item, i) =>

					<TouchableOpacity
						key={i} 
						onPress={this.handleEdit(item)}
						onLongPress={this.handleRemoveData(item.id)}
						style={styles.todolist}>
						<Text>{item.name}</Text>
					</TouchableOpacity>

				)}
				{this.props.todos.todos.length === 0 && this.handleButton()}

				{this.props.todos.todos.length === 0 &&
					<Text style={{ fontSize: 20, alignItems: 'flex-start', marginTop: 10 }}>{"Todo List Kosong"}</Text>

				}

			</View>
		);
	}
}

const DisplayView = (props) => {
	const { children, hide, style } = props
	if (hide) {
		return null
	}
	return (
		<View {...this.props} style={style}>
			{children}
		</View>
	)
}

const mapStateToProps = state => {
	return {
		todos: state.todos
	}
}

const mapDispatchToProps = dispatch => {
	return {
		addTodos: (value) => dispatch(actionTodos.addTodo(value)),
		editTodos: (value) => dispatch(actionTodos.editTodo(value)),
		removeTodos: (id) => dispatch(actionTodos.removeTodo(id)),
	}
}

const styles = StyleSheet.create({
	container: {
		flex: 1,
		paddingTop: 30,
		justifyContent: 'flex-start',
		alignItems: 'center',
	},
	TodolistText: {
		marginBottom: 10,
		fontSize: 20
	},
	inputContainer: {
		flexDirection: 'row',
		justifyContent: 'space-between',
		alignItems: 'center',
		
	},
	placeInput: {
		width: '60%',
		borderRadius: 10,
		borderWidth: 0.5,
		marginRight: 10
	},
	placeButton: {
		width: '30%',
		marginRight: 10
	},
	listContainer: {
		width: '100%'
	},
	todolist: {
		backgroundColor: 'white',
		padding: 15,
		borderBottomWidth: 1,
		borderBottomColor: 'grey',
		width: '100%'
	}
})



export default connect(mapStateToProps, mapDispatchToProps)(TodoList)